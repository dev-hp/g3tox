/* g3helper.c -- g3tox helper functions
 * Copyright 1998 Hagen Patzke
 * Portions copied and/or adapted from g3tolj.c
 */

/*
 * pm_openr( name )
 * Open input file for read access
 * Shamelessly copied from g3tolj.c
 */
FILE *pm_openr ( char *name )
{
    FILE *f;

    if (strcmp (name, "-") == 0)
        f = stdin;
    else {
        f = fopen (name, "r");
        if (f == NULL) {
            perror (name);
            exit (1);
        }/*fi illegal name*/
    }/*fi*/

    return f;
}/*pm_openr*/

/* 
 * pm_keymatch( string, keyword, minchars )
 * Match command line keywords.
 * Kind of getopt helper routine.
 * Shamelessly copied from g3tolj.c
 */
int pm_keymatch ( char *str, char *keyword, int minchars )
{
    register int len;

    len = strlen (str);
    if (len < minchars){ return (0); }

    while (--len >= 0) {
        register char c1, c2;

        c1 = *str++;
        c2 = *keyword++;
        if (c2 == '\0')    { return (0); }
        if (isupper (c1))  { c1 = tolower (c1); }
        if (isupper (c2))  { c2 = tolower (c2); }
        if (c1 != c2)      { return (0); }
    }/*endwhile*/

    return (1);
}/*pm_keymatch*/

/*
 * rawgetbit( file )
 * Get raw bit from input stream.
 * Returns 0, 1 (bit) or -1 if an error or eof occurred.
 * Stolen from g3tolj.c, heavily modified and adapted to my purposes.
 */
/* bitfield array for fast bit extraction */
#define    SHCHUNK 8
#define    SHDTYPE int
SHDTYPE    shfield[SHCHUNK] = { 0x01, 0x02, 0x04, 0x08, 
                                0x10, 0x20, 0x40, 0x80};

/* Initialization of bitfield array */
void shfield_init( int reverse )
{
  int count;
  for( count=0; count<SHCHUNK; count++ ) {
      if ( reverse )  { shfield[count]             = 1<<count; }
      else            { shfield[(SHCHUNK-1)-count] = 1<<count; }
  }/*for*/
}/*shfield_init*/

static inline int rawgetbit ( FILE* file )
{
    static SHDTYPE    shdata;
    static int shbit   = SHCHUNK;
    register int b;

#ifdef DEBUG_rawgetbit
   fprintf(stderr,".");
#endif


    /* status = already at eof? */
    if (g3_endoffile) { return (-1); }

    /* read next byte? */
    if (shbit >= SHCHUNK) {
        shdata = getc (file);
        if (shdata == EOF) {
            g3_endoffile++;
            pm_error ("Unexpected EOF in g3 file at line.", 0, 0, 0, 0, 0);
            return (-1);
        }/*fi eof*/
        shbit = 0;
    }/*fi read next byte*/

    /* get bit value from input byte and return 0/1 flag */
    b = (shdata & shfield[shbit]) ? 1:0;

#ifdef DEBUG_rawgetbit
   fprintf(stderr,"%d:%d-",shbit,b);
#endif

    shbit++;

    return (b);
}/*rawgetbit*/
